﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers
{
    [Route("api/my/[action]")]
    [ApiController]
    public class MyController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public MyController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        #region 重试
        #region GET
        /// <summary>
        /// 重试模拟
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> TestAction()
        {
            //模拟调用接口
            var client = _httpClientFactory.CreateClient("MyApi");
            var aa = await client.GetStringAsync("/api/my/RetryAction");

            aa = "2";
            return Ok(aa);
        }
        /// <summary>
        /// 重试模拟
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult RetryAction()
        {
            if (DateTime.Now.Second % 10 != 0)
                throw new Exception($"抛异常了,时间{DateTime.Now}");
            else
                return Ok(new { resCode = 1 });
        }
        #endregion
        //#region POST
        //[HttpGet]
        //public async Task<IActionResult> TestPostAction()
        //{
        //    //模拟调用接口
        //    var client = _httpClientFactory.CreateClient("MyApi");
        //    var aa = await client.PostAsync("/api/my/RetryPostAction", null);

        //    return Ok(aa);
        //}
        //[HttpPost]
        //public IActionResult RetryPostAction()
        //{
        //    throw new Exception($"抛异常了,时间{DateTime.Now}");
        //}
        //#endregion
        #endregion

        #region 超时
        /// <summary>
        /// 超时模拟
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> TestTimeOutAction()
        {
            try
            {
                //模拟调用接口
                var client = _httpClientFactory.CreateClient("MyApi");
                var aa = await client.GetStringAsync("/api/my/TimeOutAction");
            }
            catch (Exception ex)
            {
                throw new Exception($"超时了,时间{DateTime.Now}，ex:{ex.Message}");
            }
            return Ok();
        }

        /// <summary>
        /// 超时
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult TimeOutAction()
        {
            Thread.Sleep(5000);
            return Ok();
        }
        #endregion
        #region 短路
        [HttpGet]
        public async Task<IActionResult> TestBreakerAction()
        {
            //模拟调用接口
            var client = _httpClientFactory.CreateClient("MyApi");
            while (true)
            {
                try
                {
                    var aa = await client.GetStringAsync("/api/my/BreakerAction");
                }
                catch (Exception ex)
                {

                }
                Thread.Sleep(500);
            }
            return Ok();
        }
        /// <summary>
        /// 重试模拟
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult BreakerAction()
        {
            throw new Exception($"抛异常了,时间{DateTime.Now}");

            return Ok(new { resCode = 1 });
        }
        #endregion
    }
}