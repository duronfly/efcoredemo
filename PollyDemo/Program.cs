﻿using Polly;
using Polly.Timeout;
using System;
using System.Threading;

namespace PollyDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Case3();
        }
        /// <summary>
        /// FallBack => 当出现故障，则进入降级动作
        /// </summary>
        public static void Case1()
        {
            ISyncPolicy policy = Policy.Handle<ArgumentException>()
                .Fallback(() =>
                {
                    Console.WriteLine("Error occured");
                });

            policy.Execute(() =>
            {
                Console.WriteLine("Job Start");

                throw new ArgumentException("Hello Polly!");

                Console.WriteLine("Job End");
            });
        }
        /// <summary>
        /// 重试
        /// </summary>
        public static void Case2()
        {
            ISyncPolicy policy = Policy.Handle<Exception>().Retry(3);

            try
            {
                policy.Execute(() =>
                {
                    Console.WriteLine("Job Start");
                    if (DateTime.Now.Second % 10 != 0)
                    {
                        throw new Exception("Special error occured");
                    }
                    Console.WriteLine("Job End");
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("There's one unhandled exception : " + ex.Message);
            }
        }
        /// <summary>
        /// 短路保护
        /// </summary>
        public static void Case3()
        {
            // Stop for 10s after retry 6 times
            ISyncPolicy policy = Policy.Handle<Exception>()
                .CircuitBreaker(6, TimeSpan.FromSeconds(10));

            while (true)
            {
                try
                {
                    policy.Execute(() =>
                    {
                        Console.WriteLine("Job Start");
                        throw new Exception("Special error occured");
                        Console.WriteLine("Job End");
                    });
                }
                catch (Exception ex)
                {
                    Console.WriteLine("There's one unhandled exception : " + ex.Message);
                }

                Thread.Sleep(500);
            }
        }
        /// <summary>
        /// 超时
        /// </summary>
        public static void Case4()
        {
            try
            {
                ISyncPolicy policyException = Policy.Handle<TimeoutRejectedException>()
                    .Fallback(() =>
                    {
                        Console.WriteLine("Fallback");
                    });
                ISyncPolicy policyTimeout = Policy.Timeout(3, Polly.Timeout.TimeoutStrategy.Pessimistic);
                //策略封装
                ISyncPolicy mainPolicy = Policy.Wrap(policyTimeout, policyException);
                mainPolicy.Execute(() =>
                {
                    Console.WriteLine("Job Start...");
                    Thread.Sleep(5000);
                    //throw new Exception();
                    Console.WriteLine("Job End...");
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unhandled exception : {ex.GetType()} : {ex.Message}");
            }
        }
    }
}
