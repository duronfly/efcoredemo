﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDemo.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Coravel;
using EFCoreDemo.Invocables;

namespace EFCoreDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //注意：一定要加 sslmode=none 
            var connection = Configuration["DBConfig:MysqlConnection"];
            services.AddDbContext<AnalysisContext>(options =>
            {
                options.UseMySQL(connection);

            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //添加调度器
            services.AddScheduler();
            services.AddSingleton<SayHiInvocable, SayHiInvocable>();
            services.AddSingleton<QueryStudentInvocable, QueryStudentInvocable>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc(option =>
            {
                option.MapRoute(
                    name: "default",
                    template: "api/{controller}/{action}/{id?}"
                    );
            });
            var provider = app.ApplicationServices;
            provider.UseScheduler(scheduler =>
            {
                //10秒钟执行一次
                scheduler.Schedule<SayHiInvocable>().EveryTenSeconds();
                //一分钟执行一次
                scheduler.Schedule<QueryStudentInvocable>().EveryMinute();
            });
        }
    }
}
