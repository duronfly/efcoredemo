﻿using System;
using System.Collections.Generic;

namespace EFCoreDemo.Models
{
    public partial class Instructor
    {
        public Instructor()
        {
            CourseAssignment = new HashSet<CourseAssignment>();
            Department = new HashSet<Department>();
        }

        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstMidName { get; set; }
        public DateTime HireDate { get; set; }

        public virtual OfficeAssignment OfficeAssignment { get; set; }
        public virtual ICollection<CourseAssignment> CourseAssignment { get; set; }
        public virtual ICollection<Department> Department { get; set; }
    }
}
