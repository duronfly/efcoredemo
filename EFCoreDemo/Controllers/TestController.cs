﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDemo.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.Controllers
{
    public class TestController : ControllerBase
    {
        private readonly AnalysisContext _context;
        public TestController(AnalysisContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Add()
        {
            //s1实体
            var s1 = new Student
            {
                LastName = "张三",
                FirstName = "ZhangSan",
                EnrollmentDate = DateTime.Now
            };

            //s2实体
            var s2 = new Student
            {
                LastName = "李四",
                FirstName = "LiSi",
                EnrollmentDate = DateTime.Now
            };

            ////方式一：插入s1、s2
            //await _context.Student.AddAsync(s1);
            //await _context.Student.AddAsync(s2);

            //方式二：插入s1、s2
            var entitys = new List<Student>();
            entitys.Add(s1);
            entitys.Add(s2);
            await _context.Student.AddRangeAsync(entitys);

            int resCode = await _context.SaveChangesAsync();
            return this.Ok(new { resCode });
        }


        [HttpGet]
        public async Task<IActionResult> Update()
        {
            //s1实体
            var s1 = new Student
            {
                Id = 1,
                LastName = "张三",
                FirstName = "ZhangSan",
                EnrollmentDate = DateTime.Now
            };

            //s2实体
            var s2 = new Student
            {
                Id = 2,
                LastName = "李四",
                FirstName = "LiSi",
                EnrollmentDate = DateTime.Now
            };

            ////方式一：更新s1、s2
            //_context.Student.Update(s1);
            //_context.Student.Update(s2);

            //方式二：更新s1、s2
            var entitys = new List<Student>();
            entitys.Add(s1);
            entitys.Add(s2);
            _context.Student.UpdateRange(entitys);

            int resCode = await _context.SaveChangesAsync();
            return this.Ok(new { resCode });
        }

        [HttpGet]
        public async Task<IActionResult> Remove()
        {
            //s1实体
            var s1 = new Student
            {
                Id = 7
            };

            //s2实体
            var s2 = new Student
            {
                Id = 6
            };

            ////方式一：删除s1、s2
            //_context.Student.Remove(s1);
            //_context.Student.Remove(s2);

            //方式二：删除s1、s2
            var entitys = new List<Student>();
            entitys.Add(s1);
            entitys.Add(s2);
            _context.Student.RemoveRange(entitys);

            int resCode = await _context.SaveChangesAsync();
            return this.Ok(new { resCode });
        }

        [HttpGet]
        public async Task<IActionResult> GetStudent()
        {
            var dataList = await _context.Student.ToListAsync();

            return this.Ok(new { dataList });
        }

    }
}