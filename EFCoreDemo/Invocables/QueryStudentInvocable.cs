﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coravel.Invocable;
using EFCoreDemo.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EFCoreDemo.Invocables
{
    public class QueryStudentInvocable : IInvocable
    {
        private readonly AnalysisContext _context;


        public QueryStudentInvocable()
        {
            _context = new AnalysisContext();
        }
        public async Task Invoke()
        {
            var list = await _context.Student.ToListAsync();
            Console.WriteLine(list.Select(p => p.FirstName).Aggregate((x, y) => $"{x},{y}"));

        }
    }
}
