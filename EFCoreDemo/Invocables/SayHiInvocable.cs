﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coravel.Invocable;

namespace EFCoreDemo.Invocables
{
    public class SayHiInvocable : IInvocable
    {
        public Task Invoke()
        {
            Console.WriteLine("Hellow world!");
            return Task.CompletedTask;
        }
    }
}
